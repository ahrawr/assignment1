// cards.h
/* *************************************
Ricardo Salazar, February 26, 2015

Interface of a simple Card class
************************************* */

#include <string>
#include <vector>
#include <fstream>

#ifndef CARDS_H
#define CARDS_H

using namespace std;

enum suit_t { OROS, COPAS, ESPADAS, BASTOS };

/*
The values for this type start at 0 and increase by one
afterwards until they get to SIETE.
The rank reported by the function Card::get_rank() below is
the value listed here plus one.
E.g:
The rank of AS is reported as    static_cast<int>(AS) + 1   = 0 + 1 =  1
The rank of SOTA is reported as  static_cast<int>(SOTA) + 1 = 9 + 1 = 10
*/
enum rank_t { AS, DOS, TRES, CUATRO, CINCO, SEIS, SIETE, SOTA = 9, CABALLO = 10, REY = 11 };

class Card {
public:
	// Constructor assigns random rank & suit to card.
	Card();

	// Accessors 
	string get_spanish_suit() const;
	string get_spanish_rank() const;

	/*
	These are the only functions you'll need to code
	for this class. See the implementations of the two
	functions above to get an idea of how to proceed.
	*/
	string get_english_suit() const;
	string get_english_rank() const;

	// Converts card rank to number.
	// The possible returns are: 1, 2, 3, 4, 5, 6, 7, 10, 11 and 12
	int get_rank() const; 

	// Compare rank of two cards. E.g: Eight<Jack is true.
	// Assume Ace is always 1. 
	// Useful if you want to sort the cards.
	bool operator < (Card card2) const;

private:
	suit_t suit;
	rank_t rank;
};

class Player;
class Hand {
public:
	// A vector of Cards
	std::vector<Card> deck;
	Hand(); 

	// You decide what functions you'll need...
	void add_card(Player p1, Card* name_of_card);
	void deal_card(Player p1);
	void print_cards(Player p1) const;
private:
	Card* card;
	// You decide what fields you'll need...
};

class Player {
public:
	// Constructor. 
	//    Assigns initial amount of money
	Player(int m = 0) { total = 0.0; money = 0; };
	std::vector<Card> vector_cards;

	// function that will add money to player's current balance
	void addMoney(int value);
	// accessor function to return money of the player
	int getMoney() const { return money; }
	// accessor function to return bet amount
	int getBet() const { return bet; }
	// to print money
	void print_money() const;
	// mutator function to set bet
	void setBet(int b) { bet = b; }
	// accessor to get total
	double getTotal() const;
	// mutator function to set the total
	double setTotal();
	void determinetotal(Card* card);
	void setType(std::string t);
	// accessor function to get type
	std::string getType() const;
private:
	int money;
	int bet;
	double total;
	std::string type;
};

#endif