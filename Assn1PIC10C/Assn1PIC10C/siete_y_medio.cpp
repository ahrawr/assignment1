#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"
using namespace std;

// Global constants (if any)

// Non member functions declarations (if any)

void bet(Player& p1);
bool getInput();
void determinewinner(const Player& p1);
bool gameOver(const Player& p1);
// Non member functions implementations (if any)
void bet(Player& p1) {
	int initialbet;
	cin >> initialbet;
	//set bet
	p1.setBet(initialbet);
}
// to retrieve user input
bool getInput() {
	char input;
	cin >> input;
	return (input ? true : false);
}
void determinewinner(const Player& p1) {
	if (p1.getTotal() > 7.5) {
		if (p1.getType() == "User")
			std::cout << "Too bad. You lose " << p1.getBet() << "." << endl << endl;
		else
			std::cout << "You win ";// << score << std::endl;
	}
	else
		return;
}
// to test if game is over
bool gameOver(const Player& p1){
	if (p1.getMoney() == 0.0)
		return true;
	return false;
}

// Stub for main
int main() {
	// create a player
	Player player;
	// create a dealer known as 'CPU' 
	Player CPU;
	// create a hand to deal the cards
	Hand hand;
	char input = true;

	Card* c = new Card();
	// call add_card
	hand.add_card(player, c);

	player.print_money();
	bet(player);
	hand.print_cards(player);
	cin >> input;
	determinewinner(player);

	// [OUTER WHILE LOOP] while game is not over
	// [INNER WHILE LOOP] while player enters yes, bet
	while(!gameOver(player)){
		while(input){
			// print out new card
			hand.deal_card(player);
			hand.print_cards(player);
			cin >> input;
			determinewinner(player);
		}
		// now it is the CPU's turn
		hand.deal_card(CPU);
		hand.print_cards(CPU);
		determinewinner(CPU);
	}

	system("pause");
	return 0;
}