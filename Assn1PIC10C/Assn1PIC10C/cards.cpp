#include "cards.h"
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <iomanip>

/* *************************************************
Card class
************************************************* */

/*
Default constructor for the Card class.
It could give repeated cards. This is OK.
Most variations of Blackjack are played with
several decks of cards at the same time.
*/
Card::Card() {
	int r = 1 + rand() % 4;
	// to generate suits
	switch (r) {
	case 1: suit = OROS; break;
	case 2: suit = COPAS; break;
	case 3: suit = ESPADAS; break;
	case 4: suit = BASTOS; break;
	default: break;
	}
	// to generate associated rank
	r = 1 + rand() % 10;
	switch (r) {
	case  1: rank = AS; break;
	case  2: rank = DOS; break;
	case  3: rank = TRES; break;
	case  4: rank = CUATRO; break;
	case  5: rank = CINCO; break;
	case  6: rank = SEIS; break;
	case  7: rank = SIETE; break;
	case  8: rank = SOTA; break;
	case  9: rank = CABALLO; break;
	case 10: rank = REY; break;
	default: break;
	}
}

// Accessor: returns a string with the suit of the card in Spanish 
string Card::get_spanish_suit() const {
	string suitName;
	switch (suit) {
	case OROS:
		suitName = "oros";
		break;
	case COPAS:
		suitName = "copas";
		break;
	case ESPADAS:
		suitName = "espadas";
		break;
	case BASTOS:
		suitName = "bastos";
		break;
	default: break;
	}
	return suitName;
}

// Accessor: returns a string with the rank of the card in Spanish 
string Card::get_spanish_rank() const {
	string rankName;
	switch (rank) {
	case AS:
		rankName = "As";
		break;
	case DOS:
		rankName = "Dos";
		break;
	case TRES:
		rankName = "Tres";
		break;
	case CUATRO:
		rankName = "Cuatro";
		break;
	case CINCO:
		rankName = "Cinco";
		break;
	case SEIS:
		rankName = "Seis";
		break;
	case SIETE:
		rankName = "Siete";
		break;
	case SOTA:
		rankName = "Sota";
		break;
	case CABALLO:
		rankName = "Caballo";
		break;
	case REY:
		rankName = "Rey";
		break;
	default: break;
	}
	return rankName;
} 

// Accessor: returns a string with the suit of the card in English 
 string Card::get_english_suit() const {
	 string englishSuitName;
	 switch (suit) {
	 case OROS:
		 englishSuitName = "golds";
		 break;
	 case COPAS:
		 englishSuitName = "cups";
		 break;
	 case ESPADAS:
		 englishSuitName = "swords";
		 break;
	 case BASTOS:
		 englishSuitName = "clubs";
		 break;
	 default: break;
	 }
	 return englishSuitName;
}

// Accessor: returns a string with the rank of the card in English 
string Card::get_english_rank() const {
	string english_rank;
	switch (rank) {
	case AS:
		english_rank = "One";
		break;
	case DOS:
		english_rank = "Two";
		break;
	case TRES:
		english_rank = "Three";
		break;
	case CUATRO:
		english_rank = "Four";
		break;
	case CINCO:
		english_rank = "Five";
		break;
	case SEIS:
		english_rank = "Six";
		break;
	case SIETE:
		english_rank = "Seven";
		break;
	case SOTA:
		english_rank = "Ten";
		break;
	case CABALLO:
		english_rank = "Eleven";
		break;
	case REY:
		english_rank = "Twelve";
		break;
	default: break;
	}
	return english_rank;
}

// Assigns a numerical value to card based on rank.
// AS=1, DOS=2, ..., SIETE=7, SOTA=10, CABALLO=11, REY=12
int Card::get_rank() const {
	return static_cast<int>(rank) + 1;
}

// Comparison operator for cards
// Returns TRUE if card1 < card2
bool Card::operator < (Card card2) const {
	return rank < card2.rank;
}



/* *************************************************
Hand class
************************************************* */
// Implemente the member functions of the Hand class here.
Hand::Hand() {

}

// This is a helper function for the deal_card function which will deal a random card
void Hand::add_card(Player p1, Card* newcard) {
	deck.push_back(*newcard); // might not be needed
	p1.vector_cards.push_back(*newcard);
}

void Hand::deal_card(Player p1) {
	// will generate a random card
	Card* c = new Card();
	 // call add_card
	add_card(p1, c);
	std::cout << "New card: " << std::endl;
	// display the card's stats
	std::cout << setw(10) << c->get_spanish_rank() << " de " << c->get_spanish_suit() << setw(15) <<
		" ( " << c->get_english_rank() << " of " << c->get_english_suit();
}

void Hand::print_cards(Player p1) const{
	std::vector<Card>::const_iterator it;
	std::string key;
	// print out cards depending on type of player
	if (p1.getType() == "User")
		key = "Your";
	else if (p1.getType() == "CPU")
		key = "Dealer's";
	else
		key = "";

	std::cout << key << " cards:" << std::endl;
	for (it = p1.vector_cards.cbegin(); it != p1.vector_cards.cend(); ++it) 
		std::cout << setw(10) << it->get_spanish_rank() << " de " << it->get_spanish_suit() 
		<< setw(15) << " ( " << it->get_english_rank() << " of " << it->get_english_suit() << " )" << std::endl;

	if (key[0] == 'D')
		std::cout << "The dealer's total is " << p1.getTotal();
	else
		std::cout << "Your total is " << p1.getTotal() << ". Do you want another card (y/n)? ";
}

/* *************************************************
Player class
************************************************* */
// Implemente the member functions of the Player class here.
void Player::setType(std::string t) {
	type = t;
}
std::string Player::getType() const {
	return type;
}
void Player::addMoney(int added_value) {
	if (added_value > 0)
		money += added_value;
}
void Player::print_money() const {
	std::cout << "You have $" << getMoney() << ". Enter bet: ";
}
void Player::determinetotal(Card* card) {
	if (card->get_spanish_rank == SOTA || card->get_spanish_rank == CABALLO || card->get_spanish_rank == REY) {
		total += 0.5;
	}
	else {
		if (card->get_spanish_rank == AS)
			total +=  1;
		else if (card->get_spanish_rank == DOS)
			total += 2;
		else if (card->get_spanish_rank == TRES)
			total += 3;
		else if (card->get_spanish_rank == CUATRO)
			total += 4;
		else if (card->get_spanish_rank == CINCO)
			total += 5;
		else if (card->get_spanish_rank == SEIS)
			total += 6;
		else if (card->get_spanish_rank == SIETE)
			total += 7;
		else
			return;
	}
}

double Player::getTotal() const { return total; }

// might not be needed
double Player::setTotal() {
	total = 0.0;
}


